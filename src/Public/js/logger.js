$(function () {

  'use strict';

  class Logger {
    constructor() {
      this.logBlocks = Logger.findAllPre();
      this.addCloseBlock();
    }

    static findAllPre() {
      return $(document).find("pre");
    }

    addCloseBlock() {
      this.logBlocks.append('<div></div>');
    }

    static openCloseButton(log) {
      $(log).children("div").addClass("close");
    }

    static hideCloseButton(log) {
      $(log).children("div").removeClass("close");
    }

    static hideLogBlock(log) {
      $(log).slideUp();
    }
  }

  $("pre").on('mouseenter', function() {
    Logger.openCloseButton(this);
  }).on('mouseleave', function() {
    Logger.hideCloseButton(this);
  }).on('click', function () {
    Logger.hideLogBlock(this);
  });
});