<?php
/**
 * Created by PhpStorm.
 * User: FunnyCOFE
 * Company: SomniumGame
 * Date: 16.04.2019
 * Time: 22:09
 * Copyright © 2019 SomniumGame Ltd. All rights reserved
 */

namespace Logs;

class Logger
{
    /**
     * Its type of logs which mean error logs. Custom type can be added into logs_config file.
     */
    const ERROR = 0;

    /**
     * Its type of logs which mean only debug logs. Custom type can be added into logs_config file.
     */
    const LOG = 1;

    /**
     * Its type of logs which mean warnings logs. Custom type can be added into logs_config file.
     */
    const WARNING = 2;

    /** @var array|null $config */
    static private $config = null;

    /**
     * Write and show log files and cleanup them
     *
     * @param mixed $text Text for logging
     * @param integer $type Index or const attribute of log type in Config/logs_config.json
     * @param string $name Name of log file
     * @param bool $screening If true message show in var_dump()
     *
     */
    static public function Log($text, int $type, string $name, bool $screening = null): void
    {
        if (is_null(self::$config)) {
            self::$config = self::getConfig();
            self::cleanUpLogExpiredFiles();
        }

        if (is_null($screening) && $type == self::ERROR) {
            $screening = self::$config['debug_mode'];
        } elseif (is_null($screening)) {
            $screening = self::$config['debug_mode'] & self::$config[self::$config['log_types'][$type] . '_screening'];
        }

        $ip = $_SERVER['HTTP_CLIENT_IP'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ?? $_SERVER['HTTP_X_REAL_IP'] ?? $_SERVER['REMOTE_ADDR'];

        $log_file_data = self::$config['path_to_logs_dir'] . $name . '_' . self::$config['log_types'][$type] . '_' . date('d-M-Y') . '.log';
        $log_text = "CLIENT: " . $ip . " - " . date('h:i:s A') . " UTC: " . json_encode($text);
        $log_text = str_replace("\/", "/", $log_text);

        file_put_contents(__DIR__ . "/../../../../../" . $log_file_data, $log_text . "\n", FILE_APPEND);

        $class = self::prepareMessageClass($type);

        if ($screening) {
            include_once __DIR__ . "/../Public/templates/logger.html";
            echo '<pre class="' . $class . '">' . $log_file_data . ' - ', var_dump($log_text), '</pre>' . PHP_EOL;
        }
    }

    /**
     * Generate style depends of log type
     *
     * @param int $type
     * @return string
     */
    static private function prepareMessageClass(int $type): string
    {
        switch ($type) {
            case self::ERROR:
                return "error";
                break;
            case self::LOG:
                return "log";
                break;
            case self::WARNING:
                return "warning";
                break;
            default:
                return "log";
        }
    }

    /**
     * @return array Load configuration of logger
     */
    static private function getConfig(): array
    {
        return json_decode(file_get_contents(__DIR__ . "/../Configs/logs_config.json"), true);
    }

    /**
     * Check for expired logs files and deleting it
     */
    static private function cleanUpLogExpiredFiles(): void
    {
        $dir = __DIR__ . "/../../../../../" . self::$config['path_to_logs_dir'];
        if (!file_exists($dir)) {
            mkdir($dir);
        }

        $files = scandir($dir);
        foreach ($files as $file) {
            if (stripos($file, ".log") !== false) {
                $last_day = mktime(0, 0, 0, date("m"), (date("d") - self::$config['storage_time_in_days']), date("Y"));
                if (strtotime(explode('.', explode('_', $file)[2])[0]) < $last_day) {
                    if (unlink($dir . $file)) {
                        self::Log("File of log - " . $file . " was successful delete", LOGGER::LOG, "Logger");
                    } else {
                        self::Log("Could not delete file - " . $file . ". Error: " . error_get_last()['message'], LOGGER::ERROR, "Logger");
                    }
                }
            }
        }
    }
}